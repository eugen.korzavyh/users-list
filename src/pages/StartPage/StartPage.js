import React, { Component } from 'react';
import { Route, Redirect } from 'react-router-dom';
import UsersList from '../../components/UsersList/UsersList';
import UserCreation from '../../components/UserCreation/UserCreation';
import UserScreen from '../../components/UserScreen/UserScreen';

export default class StartPage extends Component {
  render() {
    return (
      <div>
        <Redirect exact from="/" to="/users-list" />
        <Route exact path="/users-list" component={UsersList} />
        <Route exact path="/user-creation/:id?" component={UserCreation} />
        <Route exact path="/user-screen/:id" component={UserScreen} />
      </div>
    );
  }
}
