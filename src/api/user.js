import axios from 'axios';

export function getUserById(id) {
  return axios.get(`http://frontend-candidate.dev.sdh.com.ua/v1/contact/${id}/`);
}

export function createNewUser(userData) {
  return axios.post('http://frontend-candidate.dev.sdh.com.ua/v1/contact/', userData);
}

export function updateUser(id, userData) {
  console.log(userData);
  return axios.put(`http://frontend-candidate.dev.sdh.com.ua/v1/contact/${id}/`, userData);
}

export function deleteUser(id) {
  return axios.delete(`http://frontend-candidate.dev.sdh.com.ua/v1/contact/${id}/`);
}
