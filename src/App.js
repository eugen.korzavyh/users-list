import React, { Component } from 'react';
import { ConnectedRouter as Router } from 'connected-react-router';
import { Switch, Route } from 'react-router-dom';
// import { connect } from "react-redux";
import { history } from './store/configureStore';
import StartPage from './pages/StartPage/StartPage';
// import getUsers from "./store/modules/users";
import './App.css';

export default class App extends Component {
  // componentDidMount() {
  //   console.log("App");
  //   this.props.getUsers()
  // }

  render() {
    return (
      <Router history={history}>
        <Switch>
          <Route path="/" component={StartPage} />
        </Switch>
      </Router>
    );
  }
}

// export default connect(null, { getUsers })(App);
