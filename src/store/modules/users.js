export const GET_USERS = 'GET_USERS';
export const GET_USERS_SUCCESS = 'GET_USERS_SUCCESS';
export const GET_USERS_FAIL = 'GET_USERS_FAIL';

const initialState = {
  users: [],
};

export default function usersReducer(state = initialState, action) {
  switch (action.type) {
    case GET_USERS: {
      return {
        ...state,
      };
    }
    case GET_USERS_SUCCESS: {
      return {
        ...state,
        users: action.data,
      };
    }
    case GET_USERS_FAIL: {
      return {
        ...state,
        error: action.error,
      };
    }
    default:
      return state;
  }
}

export function getUsers() {
  return {
    type: GET_USERS,
  };
}
