export const CREATE_USER = 'CREATE_USER';
export const CREATE_USER_SUCCESS = 'CREATE_USER_SUCCESS';
export const CREATE_USER_FAIL = 'CREATE_USER_FAIL';

export const UPDATE_USER = 'UPDATE_USER';
export const UPDATE_USER_SUCCESS = 'UPDATE_USER_SUCCESS';
export const UPDATE_USER_FAIL = 'UPDATE_USER_FAIL';

export const GET_USER = 'GET_USER';
export const GET_USER_SUCCESS = 'GET_USER_SUCCESS';
export const GET_USER_FAIL = 'GET_USER_FAIL';

export const DELETE_USER = 'DELETE_USER';
export const DELETE_USER_SUCCESS = 'DELETE_USER_SUCCESS';
export const DELETE_USER_FAIL = 'DELETE_USER_FAIL';

const initialState = {
  user: {},
};

export default function userReducer(state = initialState, action) {
  switch (action.type) {
    case CREATE_USER: {
      return {
        ...state,
      };
    }
    case CREATE_USER_SUCCESS: {
      return {
        ...state,
        user: action.user,
      };
    }
    case CREATE_USER_FAIL: {
      return {
        ...state,
        error: action.error,
      };
    }

    case UPDATE_USER: {
      return state;
    }
    case UPDATE_USER_SUCCESS: {
      return {
        ...state,
        user: action.data,
      };
    }
    case UPDATE_USER_FAIL: {
      return {
        ...state,
        error: action.error,
      };
    }

    case GET_USER: {
      return {
        ...state,
        user: {},
      };
    }
    case GET_USER_SUCCESS: {
      return {
        ...state,
        user: action.data,
      };
    }
    case GET_USER_FAIL: {
      return {
        ...state,
        error: action.error,
      };
    }

    case DELETE_USER: {
      return {
        ...state,
      };
    }
    case DELETE_USER_SUCCESS: {
      return {
        ...state,
        user: action.data,
      };
    }
    case DELETE_USER_FAIL: {
      return {
        ...state,
        error: action.error,
      };
    }
    default:
      return state;
  }
}

export function createUser(userData) {
  return {
    type: CREATE_USER,
    userData,
  };
}

export function updateUser(id, userData) {
  return {
    type: UPDATE_USER,
    id,
    userData,
  };
}

export function getUserById(id) {
  return {
    type: GET_USER,
    id,
  };
}

export function deleteUser(id) {
  return {
    type: DELETE_USER,
    id,
  };
}
