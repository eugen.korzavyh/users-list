import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import usersReducer from './users';
import userReducer from './user';

const createRootReducer = history =>
  combineReducers({
    router: connectRouter(history),
    usersReducer,
    userReducer,
  });
export default createRootReducer;
