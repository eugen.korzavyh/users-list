import { takeLatest, call, put, all } from 'redux-saga/effects';
import { GET_USERS, GET_USERS_SUCCESS, GET_USERS_FAIL } from '../modules/users';
import { getUsersList } from '../../api/users';

function* getUsersSaga() {
  try {
    const { data } = yield call(getUsersList);
    yield put({ type: GET_USERS_SUCCESS, data });
  } catch (error) {
    console.error(error);
    yield put({ type: GET_USERS_FAIL, error });
  }
}

export default function* usersSaga() {
  yield all([takeLatest(GET_USERS, getUsersSaga)]);
}
