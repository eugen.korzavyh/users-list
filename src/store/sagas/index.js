import usersSaga from './users';
import userSaga from './user';
import { all, fork } from 'redux-saga/effects';

export default function*() {
  yield all([fork(userSaga), fork(usersSaga)]);
}
