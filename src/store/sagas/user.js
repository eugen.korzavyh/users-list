import { takeLatest, call, put, all } from 'redux-saga/effects';
import {
  GET_USER,
  GET_USER_SUCCESS,
  GET_USER_FAIL,
  CREATE_USER,
  CREATE_USER_SUCCESS,
  CREATE_USER_FAIL,
  UPDATE_USER,
  UPDATE_USER_SUCCESS,
  UPDATE_USER_FAIL,
  DELETE_USER,
  DELETE_USER_SUCCESS,
  DELETE_USER_FAIL,
} from '../modules/user';
import { getUserById, createNewUser, updateUser, deleteUser } from '../../api/user';
import { GET_USERS_SUCCESS } from '../modules/users';

function* createUserSaga(action) {
  const { userData } = action;
  try {
    const response = yield call(createNewUser, userData);
    const user = response.data;

    yield put({ type: CREATE_USER_SUCCESS, user });
  } catch (error) {
    yield put({ type: CREATE_USER_FAIL, error });
  }
}

function* updateUserSaga(action) {
  try {
    const { userData } = action;
    const { user } = yield call(updateUser, userData.id, userData);

    yield put({ type: UPDATE_USER_SUCCESS, user });
  } catch (error) {
    yield put({ type: UPDATE_USER_FAIL, error });
  }
}

function* getUserSaga(action) {
  try {
    const id = action.id;
    const { data } = yield call(getUserById, id);
    yield put({ type: GET_USER_SUCCESS, data });
  } catch (error) {
    console.error(error);
    yield put({ type: GET_USER_FAIL, error });
  }
}

function* deleteUserSaga(action) {
  try {
    const { id } = action;
    yield call(deleteUser, id);
    yield put({ type: DELETE_USER_SUCCESS });
    yield put({ type: GET_USERS_SUCCESS });
  } catch (error) {
    console.error(error);
    yield put({ type: DELETE_USER_FAIL, error });
  }
}

export default function* userSaga() {
  yield all([
    takeLatest(CREATE_USER, createUserSaga),
    takeLatest(UPDATE_USER, updateUserSaga),
    takeLatest(GET_USER, getUserSaga),
    takeLatest(DELETE_USER, deleteUserSaga),
  ]);
}
