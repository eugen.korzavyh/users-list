process.env.NODE_ENV = 'development';

module.exports = {
  extends: ['react-app'],
  rules: {
    indent: 0,
    'max-len': 0,
    'no-plusplus': 0,
    'no-restricted-syntax': 0,
    'no-continue': 0,
    'react/jsx-filename-extension': 0,
    'space-before-function-paren': [2, { anonymous: 'never', named: 'never' }],
    'generator-star-spacing': 0,
    'arrow-parens': 0,
    'arrow-body-style': 0,
    'wrap-iife': 0,
    'no-mixed-operators': 0,
    'object-curly-newline': 0,
    'function-paren-newline': 0,
    'react/sort-comp': 0,
    'react/destructuring-assignment': 0,
    'react/button-has-type': 0,
    'lines-between-class-members': [2, 'always', { exceptAfterSingleLine: true }],
    'jsx-a11y/label-has-for': 0,
    'jsx-a11y/label-has-associated-control': [
      2,
      {
        labelComponents: [],
        labelAttributes: ['label'],
        controlComponents: [],
        depth: 25,
      },
    ],
    'react/jsx-one-expression-per-line': 0,
    'operator-linebreak': 0,
    'quote-props': [2, 'as-needed', { numbers: true }],
    'jsx-a11y/click-events-have-key-events': 0,
    'implicit-arrow-linebreak': 0,
    'comma-dangle': 0,
  },
  settings: {
    'import/resolver': {
      webpack: {
        config: './config/webpack.config.dev.js',
      },
    },
  },
};
