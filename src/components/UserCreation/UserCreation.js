import React, { Component } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import * as yup from 'yup';
import 'react-datepicker/dist/react-datepicker.css';
import Input from '../Input/Input';
import Text from '../Text/Text';
import Picker from '../Picker/Picker';
import Select from '../Select/Select';
import { createUser, getUserById, updateUser } from '../../store/modules/user';
import { Formik, Form, Field } from 'formik';

import './styles.css';

const UserSchema = yup.object().shape({
  first_name: yup
    .string()
    .required('please enter your name')
    .max(256, 'max 256 characters'),
  last_name: yup
    .string()
    .required('please enter your last name')
    .max(256, 'max 256 characters'),
  birth_date: yup.date().required('please enter your birth date'),
  gender: yup.string().required('please select'),
  job: yup
    .string()
    .required('this field is required')
    .max(256, 'max 256 characters'),
  biography: yup
    .string()
    .required('this field is required')
    .max(1024, 'max 1024 characters'),
});

class UserCreation extends Component {
  componentDidMount() {
    if (this.props.match.params.id) {
      this.props.getUserById(this.props.match.params.id);
    }
  }

  render() {
    const id = this.props.match.params.id;

    return (
      <Formik
        initialValues={
          this.props.user || {
            first_name: '',
            last_name: '',
            birth_date: '',
            gender: '',
            job: '',
            biography: '',
            is_active: null,
          }
        }
        validationSchema={UserSchema}
        onSubmit={user => {
          user.birth_date = moment(user.birth_date).format('YYYY-MM-DD');
          id ? this.props.updateUser(id, user) : this.props.createUser(user);
        }}
      >
        {({ errors, touched }) => (
          <Form className="container">
            <div className="userInfo">
              <Field name="first_name" component={Input} label="First name" />
              <Field name="last_name" component={Input} label="Last name" />
              <Field name="birth_date" component={Picker} label="Birth date" />
              <Field name="gender" component={Select} label="Gender" />
              <Field name="job" component={Input} label="Job" />
              <Field name="biography" component={Text} label="Biography" />
              <Field name="is_active" component={Input} label="isActive" type="checkbox" />

              <div>
                <button type="submit">submit</button>
              </div>
            </div>
          </Form>
        )}
      </Formik>
    );
  }
}

const mapStateToProps = state => ({
  user: state.userReducer.user,
});

const mapDispatchToProps = {
  createUser,
  getUserById,
  updateUser,
};

export default connect(mapStateToProps, mapDispatchToProps)(UserCreation);
