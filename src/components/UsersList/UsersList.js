import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getUsers } from '../../store/modules/users';
import { deleteUser } from '../../store/modules/user';
import { NavLink } from 'react-router-dom';

import './styles.css';

class UsersList extends Component {
  componentDidMount() {
    this.props.getUsers();
  }

  renderUsersList = () => {
    const users = this.props.users;
    return users.map(user => {
      const removeUser = () => {
        this.props.deleteUser(user.id);
      };
      return (
        <div className="user" key={user.id}>
          <NavLink to={`/user-screen/${user.id}`}>
            <div>
              {user.first_name} {user.last_name} {user.birth_date} {user.gender}
            </div>
          </NavLink>

          <button className="delBtn" onClick={removeUser}>
            del
          </button>
        </div>
      );
    });
  };

  render() {
    return (
      <div className="container">
        <div className="list">
          {this.renderUsersList()}
          <NavLink to="/user-creation">
            <button>ADD User</button>
          </NavLink>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  users: state.usersReducer.users,
});

const mapDispatchToProps = {
  getUsers,
  deleteUser,
};

export default connect(mapStateToProps, mapDispatchToProps)(UsersList);
