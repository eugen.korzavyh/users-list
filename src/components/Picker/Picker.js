import React from 'react';
import { useFormikContext } from 'formik';
import DatePicker from 'react-datepicker';

export default function Picker({ field, form, label }) {
  const { setFieldValue } = useFormikContext();
  return (
    <div className="data">
      <div className="value">
        <label className="label">{label}</label>
        <DatePicker
          {...field}
          dateFormat="yyyy/MM/dd"
          selected={(field.value && new Date(field.value)) || null}
          onChange={val => {
            setFieldValue(field.name, val);
          }}
        />
      </div>

      {!form.isValid && <div className="errorText">{form.errors[field.name]}</div>}
    </div>
  );
}
