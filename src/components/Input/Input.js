import React from 'react';

export default function Input({ field, form, label, type }) {
  const { name, onChange, value, onBlur } = field;
  return (
    <div className="data">
      <div className="value">
        <label className="label">{label}</label>
        <input
          type={type}
          name={name}
          onChange={onChange}
          value={value}
          onBlur={onBlur}
          checked={value}
        />
      </div>

      {!form.isValid && <div className="errorText">{form.errors[name]}</div>}
    </div>
  );
}
