import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { getUserById, deleteUser } from '../../store/modules/user';

import './styles.css';

class UserScreen extends Component {
  componentDidMount() {
    const id = this.props.match.params.id;
    if (id) {
      this.props.getUserById(id);
    }
  }

  removeUser = () => {
    this.props.deleteUser(this.props.user.id);
  };

  render() {
    const {
      biography,
      birth_date,
      first_name,
      gender,
      is_active,
      job,
      last_name,
      id,
    } = this.props.user;

    return (
      <div className="userData">
        <div className="info">
          <div className="item">
            <span>First name:</span>
            <span>{first_name}</span>
          </div>

          <div className="item">
            <span className="title">Last name:</span>
            <span>{last_name}</span>
          </div>

          <div className="item">
            <span className="title">Birth date:</span>
            <span>{birth_date}</span>
          </div>

          <div className="item">
            <span className="title">Gender:</span>
            <span>{gender}</span>
          </div>

          <div className="item">
            <span className="title">Biography:</span>
            <span>{biography}</span>
          </div>

          <div className="item">
            <span className="title">Is active:</span>
            <span>{is_active ? 'Active' : 'Not active'}</span>
          </div>

          <div className="item">
            <span className="title">Profession:</span>
            <span>{job}</span>
          </div>

          <div className="buttons">
            <NavLink to={`/user-creation/${id}`}>
              <button>EDIT</button>
            </NavLink>

            <button onClick={this.removeUser}>DELETE</button>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.userReducer.user,
});

const mapDispatchToProps = {
  getUserById,
  deleteUser,
};

export default connect(mapStateToProps, mapDispatchToProps)(UserScreen);
