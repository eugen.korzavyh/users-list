import React from 'react';

export default function Text({ field, form, label }) {
  const { name, onChange, value, onBlur } = field;
  return (
    <div className="data">
      <div className="value">
        <label className="label">{label}</label>
        <textarea name={name} onChange={onChange} value={value} onBlur={onBlur} />
      </div>

      {!form.isValid && <div className="errorText">{form.errors[name]}</div>}
    </div>
  );
}
