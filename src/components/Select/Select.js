import React from 'react';

export default function Select({ field, form, label }) {
  const { name, onChange, value, onBlur } = field;
  return (
    <div className="data">
      <div className="value">
        <label className="label">{label}</label>
        <select name={name} onChange={onChange} value={value} onBlur={onBlur}>
          <option value="" />
          <option value="male">Male</option>
          <option value="female">Female</option>
        </select>
      </div>

      {!form.isValid && <div className="errorText">{form.errors[name]}</div>}
    </div>
  );
}
